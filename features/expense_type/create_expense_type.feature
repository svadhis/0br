Feature: Create budget group

    Description: A user can create a budget group

    Background:
        Given a user wants to create a budget group
        And he is logged in

    Rule: The budget group name has to be unique on a budget

        Scenario: Create a budget group with a name that already exists on a budget.
            Given the user provides a budget group name that already exists on a budget
            When the user creates the budget group
            Then he gets an error
            And no new budget group is created

    Scenario: Create a budget group successfully.
        Given the user provides a budget group name that does not exist on a budget
        When the user creates the budget group
        Then the budget group is created