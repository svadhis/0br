Feature: Create budget

    Description: A user can create a budget

    Background:
        Given a user wants to create a budget
        And he is logged in

    Rule: The budget name has to be unique on his account

        Scenario: Create a budget with a name that already exists on his account.
            Given the user provides a budget name that already exists on his account
            When the user creates the budget
            Then he gets an error
            And no new budget is created

    Scenario: Create a budget successfully.
        Given the user provides a budget name that does not exist on his account
        When the user creates the budget
        Then the budget is created