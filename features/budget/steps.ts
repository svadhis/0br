import { Given, Then, When } from "@cucumber/cucumber";
import { assertThat, contains, instanceOf, is} from 'hamjest';
import CreateBudgetController, { CreateBudgetControllerProps } from "../../src/application/controllers/CreateBudgetController";
import GlobalState from "../../src/core/GlobalState";
import User from "../../src/domain/entities/User";
import Budget from "../../src/domain/entities/Budget";
import { getCreateBudgetController } from "./budget_test_depencencies";

let createBudgetController: CreateBudgetController;
let createBudgetData: CreateBudgetControllerProps;

let user = new User({ id: '0', email: 'valid@gmail.com', budgets: [new Budget({ id: '1', name: 'Default', userId: '0' })] });

Given('a user wants to create a budget', function () {
    createBudgetController = getCreateBudgetController();
});

Given('he is logged in', function () {
    GlobalState.isUserLoggedIn = true;
});

Given('the user provides a budget name that already exists on his account', function () {
    createBudgetData = {
        user: user,
        name: 'Default',
    };
});

Given('the user provides a budget name that does not exist on his account', function () {
    createBudgetData = {
        user: user,
        name: 'New Budget',
    };
});

When('the user creates the budget', async function () {
    this.useCaseStatus = await createBudgetController.use(createBudgetData);
});

Then('no new budget is created', function () {
    assertThat(user.budgets.length, is(1));
});

Then('the budget is created', function () {
    assertThat(user.budgets.length, is(2));
});