import CreateBudgetController from "../../src/application/controllers/CreateBudgetController";
import PersisterImpl from "../../src/application/persister/PersisterImpl";
import BudgetRepositoryImpl, { BudgetLocalSource } from "../../src/application/repositories/BudgetRepositoryImpl";
import BudgetCreation from "../../src/domain/actions/commands/BudgetCreation";
import ShowError from "../../src/domain/actions/outputs/ShowError";
import ShowSuccess from "../../src/domain/actions/outputs/ShowSuccess";
import { DataSource, NetworkAdapter, OperationQueue } from "../../src/domain/adapters/persister/Persister";
import BudgetRepository from "../../src/domain/adapters/repositories/BudgetRepository";
import CreateBudget from "../../src/domain/usecases/CreateBudget";
import { InMemoryBudgetSource } from "../user/user_test_dependencies";

export let createBudgetTestDependencies: CreateBudgetDependencies;

export function getCreateBudgetController() {
    let remoteDataSource = new DataSource();
    let networkAdapter = new NetworkAdapter();
    let operationQueue = new OperationQueue(remoteDataSource, networkAdapter);
    let persister = new PersisterImpl(operationQueue);
    let budgetLocalSource = new InMemoryBudgetSource();
    let budgetRepository = new BudgetRepositoryImpl(budgetLocalSource);
    let budgetCreation = new BudgetCreation(budgetRepository, persister);
    let showSuccess = new ShowSuccess();
    let showError = new ShowError();

    createBudgetTestDependencies = {
        budgetLocalSource: budgetLocalSource,
        budgetRepository: budgetRepository,
        budgetCreation: budgetCreation,
        showSuccess: showSuccess,
        createBudgetUseCase: new CreateBudget(budgetCreation, showSuccess, showError, persister)
    };

    return new CreateBudgetController(createBudgetTestDependencies.createBudgetUseCase);
}

interface CreateBudgetDependencies {
    budgetLocalSource: BudgetLocalSource;
    budgetRepository: BudgetRepository;
    budgetCreation: BudgetCreation;
    showSuccess: ShowSuccess;
    createBudgetUseCase: CreateBudget;
}