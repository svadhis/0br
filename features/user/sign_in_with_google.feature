Feature: Sign in with Google

    Background:
        Given a user wants to sign in with Google

    Rule: If no account is associated with the Google account, the account is created

        Scenario: Sign in with Google while no account is associated with the Google account
            Given no account is associated with the Google account
            When the user signs in successfully with his Google account
            Then the account is created
            And the user is signed in
            And a default budget is created

    Scenario: Fail to sign in with Google
        When the user fails to sign in with his Google account
        Then he gets an error
        And he is not signed in

    Scenario: Sign in with Google while an account is associated with the Google account
        Given an account is associated with the Google account
        When the user signs in successfully with his Google account
        Then he is signed in


