A **USER** is a user of the application.

He owns a *USER ACCOUNT* and can create *BUDGETS*.

// He can share access to his *BUDGETS* with other **USERS**, and grant them different roles and permissions.