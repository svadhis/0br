Feature: Create user

    Description: A user can sign up for an account

    Background:
        Given a user wants to sign up for an account
        And he is not logged in

    Rule: The user must provide a valid email address

        Scenario Outline: Sign up with an invalid email address
            Given the user provides an invalid <email> address
            When he submits the form
            Then he gets an error
            And no user account is created

            Examples:
                | email            | password   |
                | "notvalid"       | "abc123*"  |
                | "notvalid@"      | "abc123*"  |
                | "notvalid.com"   | "abc123*"  |
                | "notvalid@com"   | "abc123*"  |
                | "@notvalid.com"  | "abc123*"  |

    Rule: The user password must be at least 6 characters long

        Scenario: Sign up with a password that is less than 6 characters long
            Given the user provides a password that is less than 6 characters long
            When he submits the form
            Then he gets an error
            And no user account is created

    Rule: The user password must contain at least one digit and one letter and one special character

        Scenario Outline: Sign up with a password that does not contain at least one digit and one letter and one special character
            Given the user provides a <password> that does not contain at least one digit and one letter and one special character
            When he submits the form
            Then he gets an error
            And no user account is created

            Examples:
                | email                | password    |
                | "valid@gmail.com"    | "123456.*"  |
                | "valid@gmail.com"    | "123456abc" |
                | "valid@gmail.com"    | "abcdef.*"  |

    Rule: The user email must be unique

        Scenario: Sign up with an email that is already taken
            Given the user provides valid data
            And the email is already taken
            When he submits the form
            Then he gets an error
            And no new user account with this email is created

    Scenario: Sign up successfully
        Given the user provides valid data
        And the email is not already taken
        When he submits the form
        Then he is signed up
        And a default budget is created
