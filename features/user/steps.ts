import { Given, When, Then } from '@cucumber/cucumber';
import { assertThat, contains, instanceOf, is} from 'hamjest';
import SignUpController, { SignUpControllerProps } from '../../src/application/controllers/SignUpController';
import GlobalState from '../../src/core/GlobalState';
import generateGuid from '../../src/core/utils/functions';
import Budget from '../../src/domain/entities/Budget';
import User from '../../src/domain/entities/User';
import { getSignUpController, signUpTestDependencies } from './user_test_dependencies';

let signUpController: SignUpController;
let signUpData: SignUpControllerProps;

Given('a user wants to sign up for an account', function () {
    signUpController = getSignUpController();
});

Given('he is not logged in', async function () {
    GlobalState.isUserLoggedIn = false;
});

Given('the user provides an invalid {string} address', function (email: string) {
    signUpData = {
        email: email,
        password: 'abc123*'
    };
});

Given('the user provides a password that is less than 6 characters long', function () {
    signUpData = {
        email: 'valid@gmail.com',
        password: 'ab12*'
    };
});

Given('the user provides a {string} that does not contain at least one digit and one letter and one special character', function (password: string) {
    signUpData = {
        email: 'valid@gmail.com',
        password: password
    };
  });

Given('the user provides valid data', function () {
    signUpData = {
        email: 'valid@gmail.com',
        password: 'abc123*'
    };
});

Given('the email is already taken', async function () {
    await signUpTestDependencies.userRepository.createAccount(signUpData.email);
});

Given('the email is not already taken', async function () {
    await signUpTestDependencies.userRepository.deleteByEmail(signUpData.email);
});

When('he submits the form', async function () {
    this.useCaseStatus = await signUpController.use(signUpData);
});

Then('he gets an error', async function () {
    assertThat(this.useCaseStatus, is("FAILURE"));
});

Then('no user account is created', async function () {
    assertThat((await signUpTestDependencies.userRepository.getByEmail(signUpData.email)), contains(instanceOf(Error)));
});

Then('no new user account with this email is created', async function () {
    assertThat((await signUpTestDependencies.userRepository.getByEmail(signUpData.email)), instanceOf(User));
});

Then('he is signed up', async function () {
    this.user = await signUpTestDependencies.userRepository.getByEmail(signUpData.email);
    assertThat(this.useCaseStatus, is("SUCCESS"));
    assertThat(this.user, instanceOf(User));
});

Then('a default budget is created', async function () {
    assertThat((await signUpTestDependencies.budgetRepository.getByUserId(this.user.id)), contains(instanceOf(Budget)));
});