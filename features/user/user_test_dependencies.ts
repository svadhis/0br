import SignUpController from "../../src/application/controllers/SignUpController";
import UserPresenterImpl from "../../src/application/presenters/UserPresenterImpl";
import BudgetRepositoryImpl, { BudgetLocalSource } from "../../src/application/repositories/BudgetRepositoryImpl";
import UserRepositoryImpl, { UserLocalSource } from "../../src/application/repositories/UserRepositoryImpl";
import { DataSource, NetworkAdapter, OperationQueue } from "../../src/domain/adapters/persister/Persister";
import BudgetCreation from "../../src/domain/actions/commands/BudgetCreation";
import UserAccountCreation from "../../src/domain/actions/commands/UserAccountCreation";
import ShowError from "../../src/domain/actions/outputs/ShowError";
import ShowUserProfile from "../../src/domain/actions/outputs/ShowUserProfile";
import BudgetRepository from "../../src/domain/adapters/repositories/BudgetRepository";
import UserRepository from "../../src/domain/adapters/repositories/UserRepository";
import Budget from "../../src/domain/entities/Budget";
import User from "../../src/domain/entities/User";
import SignUp from "../../src/domain/usecases/SignUp";
import PersisterImpl from "../../src/application/persister/PersisterImpl";

export let signUpTestDependencies: SignUpDependencies;

export function getSignUpController() {
    let remoteDataSource = new DataSource();
    let networkAdapter = new NetworkAdapter();
    let operationQueue = new OperationQueue(remoteDataSource, networkAdapter);
    let persister = new PersisterImpl(operationQueue);
    let userLocalSource = new InMemoryUserSource();
    let budgetLocalSource = new InMemoryBudgetSource();
    let userRepository = new UserRepositoryImpl(userLocalSource);
    let userPresenter = new UserPresenterImpl();
    let budgetRepository = new BudgetRepositoryImpl(budgetLocalSource);
    let userAccountCreation = new UserAccountCreation(userRepository);
    let budgetCreation = new BudgetCreation(budgetRepository, persister);
    let showUserProfile = new ShowUserProfile(userPresenter);
    let showError = new ShowError();

    signUpTestDependencies = {
        userLocalSource: userLocalSource,
        budgetLocalSource: budgetLocalSource,
        userRepository: userRepository,
        budgetRepository: budgetRepository,
        createUserAccount: userAccountCreation,
        createBudget: budgetCreation,
        showUserProfile: showUserProfile,
        signUpUseCase: new SignUp(userAccountCreation, budgetCreation, showUserProfile, showError, persister)
    };

    return new SignUpController(signUpTestDependencies.signUpUseCase);
}

interface SignUpDependencies {
    userLocalSource: UserLocalSource;
    budgetLocalSource: BudgetLocalSource;
    userRepository: UserRepository;
    budgetRepository: BudgetRepository;
    createUserAccount: UserAccountCreation;
    createBudget: BudgetCreation;
    showUserProfile: ShowUserProfile;
    signUpUseCase: SignUp;
}

export class InMemoryUserSource implements UserLocalSource {
    private users: User[] = [];

    public async deleteByEmail(email: string): Promise<User | Error[]> {
        const user = this.users.find(u => u.email === email);
        if (user) {
            const index = this.users.indexOf(user);
            this.users.splice(index, 1);
            return user;
        }
        else {
            return [new Error('User not found')];
        }
    }

    public async create(user: User): Promise<void> {
        this.users.push(user);
    }

    public async getByEmail(email: string): Promise<User | Error[]> {
        const user = this.users.find(u => u.email === email);
        if (user) {
            return user;
        }
        else {
            return [new Error('User not found')];
        }
    }
}

export class InMemoryBudgetSource implements BudgetLocalSource {
    private budgets: Budget[] = [];

    public async create(budget: Budget): Promise<void> {
        this.budgets.push(budget);
    }

    public async delete(user: User, name: string): Promise<Budget | Error[]> {
        const budget = this.budgets.find(b => b.name === name && b.userId === user.id);
        if (budget) {
            const index = this.budgets.indexOf(budget);
            this.budgets.splice(index, 1);
            return budget;
        }
        else {
            return [new Error('Budget not found')];
        }
    }

    public async getByUserId(userId: string): Promise<Budget[] | Error[]> {
        const budgets = this.budgets.filter(b => b.userId === userId);
        if (budgets.length > 0) {
            return budgets;
        }
        else {
            return [new Error('Budgets not found')];
        }
    }

    public async get(userId: string, name: string): Promise<Budget | Error[]> {
        const budget = this.budgets.find(b => b.name === name && b.userId === userId);
        if (budget) {
            return budget;
        }
        else {
            return [new Error('Budget not found')];
        }
    }
}