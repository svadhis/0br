Feature: Create account

    Description: A user can create an account

    Background:
        Given a user wants to create an account
        And he is logged in

    Rule: The account name has to be unique on a member

        Scenario: Create an account with a name that already exists on a member.
            Given the user provides an account name that already exists on a member
            When the user creates the account
            Then he gets an error
            And no new account is created

    Scenario: Create an account successfully.
        Given the user provides an account name that does not exist on a member
        When the user creates the account
        Then the account is created