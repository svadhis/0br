An **ACCOUNT** is a money account.

It pertains to a *MEMBER*

It can be of different types :
    - Bank
    - Credit card
    - Wallet
    - Saving
    - Loan