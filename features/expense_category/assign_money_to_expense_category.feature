Feature: Budgetize budget category

    Description: A user can assign money to a budget category

    Background:
        Given a user wants to assign money to a budget category
        And he is logged in

    Scenario: Budgetize money to a budget category for the current month
        Given the budget category had not been assigned any money for the current month
        When the user assign money to the budget category for the current month
        Then the budget category should have the money assigned to it
        And the available budget should be reduced by the provided amount