Feature: Estimate budget category

    Description: A user can estimate a budget category

    Background:
        Given a user wants to estimate a budget category
        And he is logged in

    Rule: An estimation is set for every future months until one has already been estimated

        Scenario: Estimate a budget category for january 2021 while march 2021 has already been estimated
            Given the budget category has not yet been estimated for february 2021
            And the budget category has already been estimated for march 2021
            And the budget category has not yet been estimated for april 2021
            When the user estimates the budget category for january 2021
            Then january's estimation is set to the provided amount
            And february's estimation is set to the provided amount
            And march's estimation is not set to the provided amount
            And april's estimation is not set to the provided amount