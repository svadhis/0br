A **EXPENSE CATEGORY** is a category of expenses.

It's under a *EXPENSE GROUP*.

It can be a general category, and contain multiple *TRANSACTIONS*, or a specific category and have only one *TRANSACTION*.

It can be attached to a *PAYEE* and an *ACCOUNT*.

We can set it's previsional budget and assignment.