Feature: Create budget category

    Description: A user can create a budget category

    Background:
        Given a user wants to create a budget category
        And he is logged in

    Rule: The budget category name has to be unique on a budget

        Scenario: Create a budget category with a name that already exists on a budget.
            Given the user provides a budget category name that already exists on a budget
            When the user creates the budget category
            Then he gets an error
            And no new budget category is created

    Scenario: Create a budget category successfully.
        Given the user provides a budget category name that does not exist on a budget
        When the user creates the budget category
        Then the budget category is created