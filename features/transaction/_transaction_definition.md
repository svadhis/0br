A **TRANSACTION** is a flow of money.

It can be positive (inflow) or negative (outflow).

It's attached to an *ACCOUNT* and a *BUDGET CATEGORY*, as well as a *PAYEE* optionally.

It can be checked against the *ACCOUNT* it's attached to.