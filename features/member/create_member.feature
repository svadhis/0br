Feature: Create member

    Description: A user can create a member

    Background:
        Given a user wants to create a member
        And he is logged in
        And he is a premium user

    Rule: The member name has to be unique on a budget

        Scenario: Create a member with a name that already exists on a budget.
            Given the user provides a member name that already exists on a budget
            When the user creates the member
            Then he gets an error
            And no new member is created

    Scenario: Create a member successfully.
        Given the user provides a member name that does not exist on a budget
        When the user creates the member
        Then the member is created
