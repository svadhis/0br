export{}
declare global {
    interface Array<T> {
        pushWithID(fn: (id: string) => T): T;
    }
}

Array.prototype.pushWithID = function (fn): any {
    const id = this.length + 1;
    const item = fn(id.toString());
    this.push(item);
    return item;
}