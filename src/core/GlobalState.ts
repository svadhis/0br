import Action from "./abstracts/Action";

export default class GlobalState {
    public static isUserLoggedIn: boolean = false;

    public static actionsToReverse: {action: Action<any, any>, props: any}[] = [];

    public static isActionable: boolean = true;
}