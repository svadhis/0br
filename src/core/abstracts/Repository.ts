export default abstract class Repository {}

export type DataOperator<Props> = (props: Props) => Promise<void>;