import { injectable } from "inversify";
import Persister from "../../domain/adapters/persister/Persister";
import Action, { ErrorOutputAction, OptionalCommandAction, OptionalQueryAction, OptionalVerification, OutputAction, Verification } from "./Action";


export default abstract class UseCase<Props> {
    public status: UseCaseStatus | undefined;
    public optionalErrors: Error[] = [];
    public abstract useConditions: UseCondition<Props>[];

    public noActionFailed: boolean = true;

    protected persister: Persister;
    public showError: ErrorOutputAction;

    constructor(persister: Persister, showError: ErrorOutputAction) {
        this.persister = persister;
        this.showError = showError;
    }

    private reset(): void {
        this.status = undefined;
        this.optionalErrors = [];
        this.noActionFailed = true;
    }

    public async execute(props: Props): Promise<UseCaseStatus> {
        if ((await this.isLeggit(props))) {
            await this._execute(props);
        }
        const result = this.status;
        this.reset();

        return result ?? "FAILURE";
    }

    protected abstract _execute(props: Props): Promise<void>;

    protected async verify<VerificationProps>(action: Verification<VerificationProps>, props: VerificationProps): Promise<boolean> {
        if (this.noActionFailed) {
            const actionResult = await action.execute(props);

            if (await this.isError(actionResult)) {
                if (!(action instanceof OptionalVerification)) {
                    this.noActionFailed = false;
                    await this.outputError(actionResult as Error[]);
                }
                else {
                    this.optionalErrors.push(...actionResult as Error[]);
                }
            }
            else {
                return actionResult as boolean;
            }
        }

        return false;
    }

    protected async useAction<ActionProps, ActionData>(action: Action<ActionProps, ActionData>, props: ActionProps): Promise<ActionData | Error[] | void> {
        if (this.noActionFailed) {
            const actionResult = await action.execute(props);

            if (await this.isError(actionResult)) {
                if (!(action instanceof OptionalCommandAction) && !(action instanceof OptionalQueryAction)) {
                    this.noActionFailed = false;
                    await this.outputError(actionResult as Error[]);
                }
                else {
                    this.optionalErrors.push(...actionResult as Error[]);
                }
            }
            else {
                return actionResult;
            }
        }
    }

    public async outputError(data: Error[]): Promise<void> {
        this.status = "FAILURE";
        return this.showError.execute(data);
    }

    public async output<OutputProps>(action: OutputAction<OutputProps>, data: OutputProps): Promise<void> {
        if (this.noActionFailed) {
            this.status = "SUCCESS";
            await action.execute(data);
            await this.persister.save();
        }
        else {
            this.persister.clearOperations();
        }
    }

    private async isLeggit(props: Props): Promise<boolean> {
        const result = await Promise.all(this.useConditions.map(condition => condition(props)));
        return result.every(condition => condition === true);
    }

    protected async isError(data: any): Promise<boolean> {
        const errorData = await data;
        return (Array.isArray(errorData) && errorData.every(item => item instanceof Error));
    }
}

export type UseCaseStatus = "FAILURE" | "SUCCESS";

export type UseCondition<P> = (props: P) => boolean | Promise<boolean>;

export class ActionTaken<Props, ActionData> {
    public action: Action<Props, ActionData>;
    public props: Props;
    public data: ActionData;

    constructor(action: Action<Props, ActionData>, props: Props, data: ActionData) {
        this.action = action;
        this.props = props;
        this.data = data;
    }
}