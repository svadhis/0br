import { inject } from "inversify";

export default abstract class Action<Props, SuccessData> {
    public abstract businessRuleValidators: BusinessRuleValidatorConstructor<Props>[];
    public errors: Error[] = [];

    private reset(): void {
        this.errors = [];
    }

    public async execute(props: Props): Promise<SuccessData | Error[]> {
        if ((await this.isValid(props))) {
            return this._execute(props);
        }
        else {
            const errors = this.errors;
            this.reset();
            return errors;
        }
    }

    protected abstract _execute(props: Props): Promise<SuccessData | Error[]>;

    // public abstract revert(data: SuccessData, props: Props): Promise<SuccessData | null>;

    private async isValid(props: Props): Promise<boolean> {
        for await (const validatorConstructor of this.businessRuleValidators) {
            const validator = validatorConstructor(props);

            if ((await validator.hasFailed())) {
                this.errors.push(validator.getError());
            }
        }

        if (this.errors.length > 0) {
            return false;
        }

        return true;
    }

    protected async isError(data: any): Promise<boolean> {
        const errorData = await data;
        return (Array.isArray(errorData) && errorData.every(item => item instanceof Error));
    }
}

export abstract class OutputAction<P> {
    public abstract execute(props: P): Promise<void>;
}

export abstract class ErrorOutputAction extends OutputAction<Error[]> {
    public abstract execute(props: Error[]): Promise<void>;
}

export abstract class QueryAction<P, S> extends Action<P, S> {}

export abstract class OptionalQueryAction<P, S> extends QueryAction<P, S> {}

export abstract class Verification<P> extends QueryAction<P, boolean> {}

export abstract class OptionalVerification<P> extends Verification<P> {}

export abstract class CommandAction<P, S> extends Action<P, S> {}

export abstract class OptionalCommandAction<P, S> extends CommandAction<P, S> {}

export type BusinessRuleValidatorConstructor<P> = (props: P) => BusinessRuleValidator;

export type Success = true;

export class BusinessRuleValidator {
    public condition: boolean | Promise<boolean>;
    private errorMessage: string;

    constructor({condition, errorMessage}: {condition: boolean | Promise<boolean>, errorMessage: string}) {
        this.condition = condition;
        this.errorMessage = errorMessage;
    }

    public async hasFailed(): Promise<boolean> {
        const result = await this.condition;
        return !result;
    }

    public getError(): Error {
        return new Error(this.errorMessage);
    }
}