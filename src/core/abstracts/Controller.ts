import GlobalState from "../GlobalState";
import UseCase, { UseCaseStatus } from "./UseCase";

export default abstract class Controller<Props, UseCaseProps> {
    protected abstract dataValidators: DataValidatorConstructor<Props>[];
    protected useCase: UseCase<UseCaseProps>;

    constructor(useCase: UseCase<UseCaseProps>) {
        this.useCase = useCase;
    }

    public errors: Error[] = [];

    protected abstract formatData(data: Props): UseCaseProps;

    public async use(props: Props): Promise<UseCaseStatus> {
        if (!GlobalState.isActionable) return "FAILURE";

        if ((await this.isValid(props))) {
            GlobalState.isActionable = false;
            const data = this.formatData(props);
            const useCaseStatus = await this.useCase.execute(data);
            GlobalState.isActionable = true;
            return useCaseStatus;
        }
        else {
            return "FAILURE";
        }
    }

    private async isValid(props: Props): Promise<boolean> {
        for await (const validatorConstructor of this.dataValidators) {
            const validator =  validatorConstructor(props);

            if ((await validator.hasFailed())) {
                this.errors.push(validator.getError());
            }
        }

        if (this.errors.length > 0) {
            return false;
        }

        return true;
    }
}

export type DataValidatorConstructor<P> = (props: P) => DataValidator;

export class DataValidator {
    public condition: boolean | Promise<boolean>;
    private errorMessage: string;

    constructor({condition, errorMessage}: {condition: boolean | Promise<boolean>, errorMessage: string}) {
        this.condition = condition;
        this.errorMessage = errorMessage;
    }

    public async hasFailed(): Promise<boolean> {
        return !this.condition;
    }

    public getError(): Error {
        return new Error(this.errorMessage);
    }
}