import CreateBudgetController from "../application/controllers/CreateBudgetController";
import { BudgetLocalSourceImpl } from "../application/data/local/BudgetLocalSourceImpl";
import PersisterImpl from "../application/persister/PersisterImpl";
import BudgetRepositoryImpl, { BudgetLocalSource } from "../application/repositories/BudgetRepositoryImpl";
import BudgetCreation from "../domain/actions/commands/BudgetCreation";
import ShowError from "../domain/actions/outputs/ShowError";
import ShowSuccess from "../domain/actions/outputs/ShowSuccess";
import { DataSource, NetworkAdapter, OperationQueue } from "../domain/adapters/persister/Persister";
import BudgetRepository from "../domain/adapters/repositories/BudgetRepository";
import CreateBudget from "../domain/usecases/CreateBudget";

export let createBudgetTestDependencies: CreateBudgetDependencies;

export function getCreateBudgetController() {
    let remoteDataSource = new DataSource();
    let networkAdapter = new NetworkAdapter();
    let operationQueue = new OperationQueue(remoteDataSource, networkAdapter);
    let persister = new PersisterImpl(operationQueue);
    let budgetLocalSource = new BudgetLocalSourceImpl();
    let budgetRepository = new BudgetRepositoryImpl(budgetLocalSource);
    let budgetCreation = new BudgetCreation(budgetRepository, persister);
    let showSuccess = new ShowSuccess();
    let showError = new ShowError();

    createBudgetTestDependencies = {
        budgetLocalSource: budgetLocalSource,
        budgetRepository: budgetRepository,
        budgetCreation: budgetCreation,
        showSuccess: showSuccess,
        createBudgetUseCase: new CreateBudget(budgetCreation, showSuccess, showError, persister)
    };

    return new CreateBudgetController(createBudgetTestDependencies.createBudgetUseCase);
}

interface CreateBudgetDependencies {
    budgetLocalSource: BudgetLocalSource;
    budgetRepository: BudgetRepository;
    budgetCreation: BudgetCreation;
    showSuccess: ShowSuccess;
    createBudgetUseCase: CreateBudget;
}