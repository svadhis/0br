import "reflect-metadata";
import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useInjection, Provider } from "inversify-react";
import CreateBudgetController from "./application/controllers/CreateBudgetController";
import User from "./domain/entities/User";
import { getCreateBudgetController } from './core/di';

function App() {

  const createBudgetController: CreateBudgetController = getCreateBudgetController();

  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p onClick={() => createBudgetController.use({
            user: new User({
              id: '1',
              email: 'imeian@gmail.com',
              budgets: []
            }),
            name: "Budget test"
          })}>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="text-yellow-500"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React !
          </a>
        </header>
      </div>
  );
}

export default App;
