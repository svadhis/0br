import { injectable } from "inversify";
import { ShowUserProfileProps } from "../../domain/actions/outputs/ShowUserProfile";
import UserPresenter from "../../domain/adapters/presenters/UserPresenter";


export default class UserPresenterImpl implements UserPresenter {
    public async showUserProfile(props: ShowUserProfileProps): Promise<void> {}
}