import Budget from "../../../domain/entities/Budget";
import User from "../../../domain/entities/User";
import { BudgetLocalSource } from "../../repositories/BudgetRepositoryImpl";

export class BudgetLocalSourceImpl implements BudgetLocalSource {
    private budgets: Budget[] = [];

    public async create(budget: Budget): Promise<void> {
        this.budgets.push(budget);
    }

    public async delete(user: User, name: string): Promise<Budget | Error[]> {
        const budget = this.budgets.find(b => b.name === name && b.userId === user.id);
        if (budget) {
            const index = this.budgets.indexOf(budget);
            this.budgets.splice(index, 1);
            return budget;
        }
        else {
            return [new Error('Budget not found')];
        }
    }

    public async getByUserId(userId: string): Promise<Budget[] | Error[]> {
        const budgets = this.budgets.filter(b => b.userId === userId);
        if (budgets.length > 0) {
            return budgets;
        }
        else {
            return [new Error('Budgets not found')];
        }
    }

    public async get(userId: string, name: string): Promise<Budget | Error[]> {
        const budget = this.budgets.find(b => b.name === name && b.userId === userId);
        if (budget) {
            return budget;
        }
        else {
            return [new Error('Budget not found')];
        }
    }
}