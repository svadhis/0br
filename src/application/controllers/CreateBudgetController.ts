import { injectable } from "inversify";
import Controller, { DataValidator, DataValidatorConstructor } from "../../core/abstracts/Controller";
import User from "../../domain/entities/User";
import CreateBudget, { CreateBudgetProps } from "../../domain/usecases/CreateBudget";


export default class CreateBudgetController extends Controller<CreateBudgetControllerProps, CreateBudgetProps> {
    protected dataValidators: DataValidatorConstructor<CreateBudgetControllerProps>[] = [
        ({ name }) => new DataValidator({
            condition: this.nameIsValid(name),
            errorMessage: 'Name is required',
        }),
    ];

    constructor(useCase: CreateBudget) {
        super(useCase);
    }

    protected formatData(data: CreateBudgetControllerProps): CreateBudgetProps {
        return data;
    }

    protected nameIsValid(name: string): boolean {
        return name.length > 0;
    }
}

export interface CreateBudgetControllerProps {
    user: User;
    name: string;
}