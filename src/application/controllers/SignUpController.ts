import { injectable } from "inversify";
import Controller, { DataValidator, DataValidatorConstructor } from "../../core/abstracts/Controller";
import SignUp, { SignUpProps } from "../../domain/usecases/SignUp";


export default class SignUpController extends Controller<SignUpControllerProps, SignUpProps> {
    protected dataValidators: DataValidatorConstructor<SignUpControllerProps>[] = [
        ({ email }) => new DataValidator({
            condition: this.emailIsValid(email),
            errorMessage: "Invalid email format"
        }),
        ({ password }) => new DataValidator({
            condition: this.passwordIsValid(password),
            errorMessage: "Password must be at least 6 characters long and contain at least one number, one letter and one special character"
        })
    ];

    constructor(useCase: SignUp) {
        super(useCase);
    }

    protected formatData(data: SignUpControllerProps): SignUpProps {
        return data;
    }

    private emailIsValid(email: string): boolean {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
    }

    private passwordIsValid(password: string): boolean {
        return password.match(/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,}$/) !== null;
    }
}

export interface SignUpControllerProps {
    email: string;
    password: string;
}