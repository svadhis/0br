import { DataOperator } from "../../core/abstracts/Repository";
import Persister, { DataOperation, OperationQueue } from "../../domain/adapters/persister/Persister";

export default class PersisterImpl implements Persister {
    protected operations: DataOperation<any>[] = [];

    constructor(
        private operationQueue: OperationQueue,
    ) {}

    public addOperation<T>(operation: DataOperator<T>, payload: T): void {
        const dataOperation = new DataOperation<T>(operation, payload);
        this.operations.push(dataOperation);
    }

    public clearOperations(): void {
        this.operations = [];
    }

    public async save(): Promise<void> {
        for await (const operation of this.operations) {
            await operation.operator(operation.payload);
        }

        this.clearOperations();
    }
}