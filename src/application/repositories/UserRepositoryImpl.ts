import { inject, injectable } from "inversify";
import { DataOperator } from "../../core/abstracts/Repository";
import UserRepository from "../../domain/adapters/repositories/UserRepository";
import User from "../../domain/entities/User";

export abstract class UserLocalSource {
    public abstract create(user: User): Promise<void>;
    public abstract getByEmail(email: string): Promise<User | Error[]>;
    public abstract deleteByEmail(email: string): Promise<User | Error[]>;
}


export default class UserRepositoryImpl implements UserRepository {
    constructor(
          private userLocalSource: UserLocalSource,
    ) {}

    public createAccount(email: string): Promise<User | Error[]> {
        throw new Error("Method not implemented.");
    }

    public login(email: string, accessToken: string): Promise<User | Error[]> {
        throw new Error("Method not implemented.");
    }

    public checkAccount(email: string): Promise<boolean | Error[]> {
        throw new Error("Method not implemented.");
    }

    public create: DataOperator<User> = async (user: User): Promise<void> => {
        await this.userLocalSource.create(user);
    };

    public async deleteByEmail(email: string): Promise<User | Error[]> {
        return this.userLocalSource.deleteByEmail(email);
    }

    public async getByEmail(email: string): Promise<User | Error[]> {
        return this.userLocalSource.getByEmail(email);
    }
}