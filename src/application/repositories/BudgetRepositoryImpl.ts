import { inject, injectable } from "inversify";
import { DataOperator } from "../../core/abstracts/Repository";
import BudgetRepository from "../../domain/adapters/repositories/BudgetRepository";
import Budget from "../../domain/entities/Budget";
import User from "../../domain/entities/User";

export abstract class BudgetLocalSource {
    public abstract create(budget: Budget): Promise<void>;
    public abstract delete(user: User, name: string): Promise<Budget | Error[]>;
    public abstract getByUserId(userId: string): Promise<Budget[] | Error[]>;
    public abstract get(userId: string, name: string): Promise<Budget | Error[]>;
}


export default class BudgetRepositoryImpl implements BudgetRepository {
    constructor(
          private budgetLocalSource: BudgetLocalSource,
    ) {}

    public create: DataOperator<Budget> = async (budget: Budget): Promise<void> => {
        await this.budgetLocalSource.create(budget);
    }

    public delete(user: User, name: string): Promise<Budget | Error[]> {
        return this.budgetLocalSource.delete(user, name);
    }

    public async getByUserId(userId: string): Promise<Budget[] | Error[]> {
        return this.budgetLocalSource.getByUserId(userId);
    }

    public async get(userId: string, name: string): Promise<Budget | Error[]> {
        return this.budgetLocalSource.get(userId, name);
    }
}