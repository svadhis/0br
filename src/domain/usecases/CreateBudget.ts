import { inject, injectable } from "inversify";
import Persister from "../adapters/persister/Persister";
import UseCase, { UseCondition } from "../../core/abstracts/UseCase";
import GlobalState from "../../core/GlobalState";
import BudgetCreation from "../actions/commands/BudgetCreation";
import ShowError from "../actions/outputs/ShowError";
import User from "../entities/User";
import ShowSuccess from "../actions/outputs/ShowSuccess";


export default class CreateBudget extends UseCase<CreateBudgetProps> {
    public useConditions: UseCondition<CreateBudgetProps>[] = [
        // (_) => GlobalState.isUserLoggedIn, // ! COMMENTED FOR BUDGET CREATION TESTS ONLY
    ];

    constructor(
          private budgetCreation: BudgetCreation,
          private showSuccess: ShowSuccess,
          showError: ShowError,
          persister: Persister,
    ) { super(persister, showError) }

    protected async _execute({ user, name }: CreateBudgetProps): Promise<void> {
        await this.useAction(this.budgetCreation, { user: user, name: name });

        await this.output(this.showSuccess, 'Budget' + name + 'created succesfully');
    }
}

export interface CreateBudgetProps {
    user: User;
    name: string;
}