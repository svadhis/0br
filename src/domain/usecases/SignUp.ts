import { inject, injectable } from "inversify";
import Persister from "../../domain/adapters/persister/Persister";
import UseCase, { UseCondition } from "../../core/abstracts/UseCase";
import GlobalState from "../../core/GlobalState";
import BudgetCreation from "../actions/commands/BudgetCreation";
import UserAccountCreation from "../actions/commands/UserAccountCreation";
import ShowError from "../actions/outputs/ShowError";
import ShowUserProfile, { ShowUserProfileProps } from "../actions/outputs/ShowUserProfile";
import User from "../entities/User";


export default class SignUp extends UseCase<SignUpProps> {
    public useConditions: UseCondition<SignUpProps>[] = [
        (_) => GlobalState.isUserLoggedIn === false,
    ];

    constructor(
          private userAccountCreation: UserAccountCreation,
          private budgetCreation: BudgetCreation,
          private showUserProfile: ShowUserProfile,
          showError: ShowError,
          persister: Persister,
    ) { super(persister, showError) }

    protected async _execute({ email, password }: SignUpProps): Promise<void> {
        const newUser = await this.useAction(this.userAccountCreation, { email });
        const defaultBudget = await this.useAction(this.budgetCreation, { user: newUser as User, name: 'Default' });

        await this.output(this.showUserProfile, { user: newUser as User });
    }
}

export interface SignUpProps {
    email: string;
    password: string;
}