import { inject, injectable } from "inversify";
import UseCase, { UseCondition } from "../../core/abstracts/UseCase";
import BudgetCreation from "../actions/commands/BudgetCreation";
import GoogleConnection, { GoogleConnectionData } from "../actions/commands/GoogleConnection";
import UserAccountCreation from "../actions/commands/UserAccountCreation";
import UserAccountLogin from "../actions/commands/UserAccountLogin";
import ShowBudget from "../actions/outputs/ShowBudget";
import ShowError from "../actions/outputs/ShowError";
import ShowUserProfile from "../actions/outputs/ShowUserProfile";
import UserAccountVerification from "../actions/queries/UserAccountVerification";
import Persister from "../adapters/persister/Persister";
import User from "../entities/User";


export default class SignInWithGoogle extends UseCase<null> {
    public useConditions: UseCondition<null>[] = [];

    constructor(
          private googleConnection: GoogleConnection,
          private userAccountVerification: UserAccountVerification,
          private userAccountCreation: UserAccountCreation,
          private budgetCreation: BudgetCreation,
          private userAccountLogin: UserAccountLogin,
          private showUserProfile: ShowUserProfile,
          private showBudget: ShowBudget,
          showError: ShowError,
          persister: Persister,
    ) { super(persister, showError); }

    protected async _execute(): Promise<void> {
        const googleConnectionData = await this.useAction(this.googleConnection, null);

        const isNewUser = await this.verify(this.userAccountVerification, { email: (googleConnectionData as GoogleConnectionData).email });

        if (isNewUser) {
            const newUser = await this.useAction(this.userAccountCreation, { email: (googleConnectionData as GoogleConnectionData).email });
            const newDefaultBudget = await this.useAction(this.budgetCreation, { user: newUser as User, name: 'Default' });
        }

        const user = await this.useAction(this.userAccountLogin, googleConnectionData as GoogleConnectionData);

        if (isNewUser) {
            await this.output(this.showUserProfile, { user: user as User });
        }
        else {
            await this.output(this.showBudget, { user: user as User });
        }
    }
}

