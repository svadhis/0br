import Repository, { DataOperator } from "../../../core/abstracts/Repository";
import Budget from "../../entities/Budget";
import User from "../../entities/User";

export default abstract class BudgetRepository extends Repository {
    public abstract create: DataOperator<Budget>;
    public abstract delete(user: User, name: string): Promise<Budget | Error[]>;
    public abstract getByUserId(userId: string): Promise<Budget[] | Error[]>;
    public abstract get(userId: string, name: string): Promise<Budget | Error[]>;
}
