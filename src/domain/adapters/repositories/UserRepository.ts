import Repository, { DataOperator } from "../../../core/abstracts/Repository";
import User from "../../entities/User";

export default abstract class UserRepository extends Repository {
    // public abstract create: DataOperator<User>;
    public abstract getByEmail(email: string): Promise<User | Error[]>;
    public abstract deleteByEmail(email: string): Promise<User | Error[]>;
    public abstract createAccount(email: string): Promise<User | Error[]>;
    public abstract login(email: string, accessToken: string): Promise<User | Error[]>;
    public abstract checkAccount(email: string): Promise<boolean | Error[]>;
}
