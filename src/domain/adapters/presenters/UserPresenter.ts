import Presenter from "../../../core/abstracts/Presenter";
import { ShowUserProfileProps } from "../../actions/outputs/ShowUserProfile";
import User from "../../entities/User";

export default abstract class UserPresenter extends Presenter {
    public abstract showUserProfile(props: ShowUserProfileProps): Promise<void>;
}