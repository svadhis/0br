import { injectable } from "inversify";
import { GoogleConnectionData } from "../../actions/commands/GoogleConnection";

injectable()
export default abstract class GoogleBridge {
    public abstract connect(): Promise<GoogleConnectionData | Error[]>;
}