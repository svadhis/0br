import { inject, injectable } from "inversify";
import { DataOperator } from "../../../core/abstracts/Repository";

export class DataSource {
    public async save(data: DataOperation<any>[]): Promise<true | Error> {
        return true;
    };
}

export class NetworkAdapter {
    public isOnline: boolean = false;
}

export default abstract class Persister {
    public abstract save(): Promise<void>;
    public abstract addOperation<T>(operation: DataOperator<T>, payload: T): void;
    public abstract clearOperations(): void;
}

export class DataOperation<T> {
    public operator: DataOperator<T>;
    public payload: T;

    constructor (operator: DataOperator<T>, payload: T) {
        this.operator = operator;
        this.payload = payload;
    }
}

export class OperationQueue {
    public operations: DataOperation<any>[] = [];

    constructor(
        private remoteDataSource: DataSource,
        private networkAdapter: NetworkAdapter,
    ) {}

    public addOperations(operations: DataOperation<any>[]): void {
        this.operations = this.operations.concat(operations);
    }

    public persistOperations(): void {
        if (this.networkAdapter.isOnline) {
            this.remoteDataSource.save(this.operations);
        }
    }


    public removeOperation(operation: DataOperation<any>): void {
        this.operations = this.operations.filter(c => c !== operation);
    }



    public clearOperations(): void {
        this.operations = [];
    }
}