import { inject, injectable } from "inversify";
import { BusinessRuleValidatorConstructor, QueryAction, Verification } from "../../../core/abstracts/Action";
import UserRepository from "../../adapters/repositories/UserRepository";

injectable()
export default class UserAccountVerification extends Verification<UserAccountVerificationProps> {
    public businessRuleValidators: BusinessRuleValidatorConstructor<UserAccountVerificationProps>[] = [];

    constructor(
          private userRepository: UserRepository,
    ) { super(); }

    protected async _execute({ email }: UserAccountVerificationProps): Promise<boolean | Error[]> {
        return this.userRepository.checkAccount(email);
    }
}

export interface UserAccountVerificationProps {
    email: string;
}