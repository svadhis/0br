import { OutputAction } from "../../../core/abstracts/Action";

export default class ShowSuccess extends OutputAction<string> {
    public async execute(message: string): Promise<void> {}
}