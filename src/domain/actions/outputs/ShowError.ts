import { ErrorOutputAction, OutputAction } from "../../../core/abstracts/Action";

export default class ShowError extends ErrorOutputAction {
    public async execute(props: Error[]): Promise<void> {}
}