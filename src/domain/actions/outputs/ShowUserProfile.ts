import { inject, injectable } from "inversify";
import { OutputAction } from "../../../core/abstracts/Action";
import UserPresenter from "../../adapters/presenters/UserPresenter";
import User from "../../entities/User";


export default class ShowUserProfile extends OutputAction<ShowUserProfileProps> {
    constructor(
          private userPresenter: UserPresenter,
    ) { super(); }

    public async execute(props: ShowUserProfileProps): Promise<void> {
        await this.userPresenter.showUserProfile(props);
    }
}

export interface ShowUserProfileProps {
    user: User;
}