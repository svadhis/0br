import { injectable } from "inversify";
import { OutputAction } from "../../../core/abstracts/Action";
import User from "../../entities/User";

injectable()
export default class ShowBudget extends OutputAction<ShowBudgetProps> {
    public execute(props: ShowBudgetProps): Promise<void> {
        throw new Error("Method not implemented.");
    }
}

export interface ShowBudgetProps {
    user: User;
    budgetId?: string;
}