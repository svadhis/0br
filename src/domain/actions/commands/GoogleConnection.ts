import { inject, injectable } from "inversify";
import { BusinessRuleValidatorConstructor, CommandAction } from "../../../core/abstracts/Action";
import GoogleBridge from "../../adapters/bridges/GoogleBridge";


export default class GoogleConnection extends CommandAction<null, GoogleConnectionData> {
    public businessRuleValidators: BusinessRuleValidatorConstructor<null>[] = [];

    constructor(
          private googleBridge: GoogleBridge,
    ) { super(); }

    protected async _execute(): Promise<GoogleConnectionData | Error[]> {
        return this.googleBridge.connect();
    }
}

export class GoogleConnectionData {
    public email: string;
    public accessToken: string;

    constructor(email: string, accessToken: string) {
        this.email = email;
        this.accessToken = accessToken;
    }
}