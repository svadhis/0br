import { inject, injectable } from "inversify";
import { BusinessRuleValidatorConstructor, CommandAction } from "../../../core/abstracts/Action";
import UserRepository from "../../adapters/repositories/UserRepository";
import User from "../../entities/User";

injectable()
export default class UserAccountLogin extends CommandAction<UserAccountLoginProps, User> {
    public businessRuleValidators: BusinessRuleValidatorConstructor<UserAccountLoginProps>[] = [];

    constructor(
          private userRepository: UserRepository,
    ) { super(); }

    protected async _execute({ email, accessToken }: UserAccountLoginProps): Promise<User | Error[]> {
        return this.userRepository.login(email, accessToken);
    }
}

export interface UserAccountLoginProps {
    email: string;
    accessToken: string;
}