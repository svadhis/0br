import { inject, injectable } from "inversify";
import { CommandAction, BusinessRuleValidatorConstructor, BusinessRuleValidator } from "../../../core/abstracts/Action";
import Persister from "../../adapters/persister/Persister";
import BudgetRepository from "../../adapters/repositories/BudgetRepository";
import Budget from "../../entities/Budget";
import User from "../../entities/User";


export default class BudgetCreation extends CommandAction<BudgetCreationProps, Budget> {
    public businessRuleValidators: BusinessRuleValidatorConstructor<BudgetCreationProps>[] = [
        ({ user, name }) => new BusinessRuleValidator({
            condition: this.nameIsNotUsed(user, name),
            errorMessage: "Name already used",
        })
    ];

    constructor(
          private budgetRepository: BudgetRepository,
          private persister: Persister,
    ) { super(); }

    protected async _execute({ user, name }: BudgetCreationProps): Promise<Budget> {
        const budget = new Budget({ name: name, userId: user.id });
        user.addBudget(budget);
        this.persister.addOperation(this.budgetRepository.create, budget);

        return budget;
    }

    private async nameIsNotUsed(user: User, name: string): Promise<boolean> {
        return user.budgets.find(budget => budget.name === name) === undefined;
    }
}

export interface BudgetCreationProps {
    user: User;
    name: string;
}