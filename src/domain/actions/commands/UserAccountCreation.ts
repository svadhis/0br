import { inject, injectable } from "inversify";
import { CommandAction, BusinessRuleValidatorConstructor } from "../../../core/abstracts/Action";
import UserRepository from "../../adapters/repositories/UserRepository";
import User from "../../entities/User";


export default class UserAccountCreation extends CommandAction<UserAccountCreationProps, User> {
    public businessRuleValidators: BusinessRuleValidatorConstructor<UserAccountCreationProps>[] = [];

    constructor(
          private userRepository: UserRepository,
    ) { super(); }

    public async _execute({ email }: UserAccountCreationProps): Promise<User | Error[]> {
        return this.userRepository.createAccount(email);
    }
}

export interface UserAccountCreationProps {
    email: string;
}