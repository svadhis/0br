import generateGuid from "../../core/utils/functions";

export default class Transaction {
    public id: string;
    public amount: number;

    constructor({ id, amount }: TransactionProps) {
        this.id = id ?? generateGuid();
        this.amount = amount;
    }
}

interface TransactionProps {
    id?: string;
    amount: number;
}