import generateGuid from "../../core/utils/functions";
import MonthlyBudget from "./MonthlyBudget";
import Transaction from "./Transaction";

export default class ExpenseCategory {
    public id: string;
    public name: string;
    public expenseTypeId: string;
    public monthlyBudgets: MonthlyBudget[];

    constructor({ id, name, budgetGroupId, monthlyBudgets }: ExpenseCategoryProps) {
        this.id = id ?? generateGuid();
        this.name = name;
        this.expenseTypeId = budgetGroupId;
        this.monthlyBudgets = monthlyBudgets ?? [];
    }
}

interface ExpenseCategoryProps {
    id?: string;
    name: string;
    budgetGroupId: string;
    monthlyBudgets?: MonthlyBudget[];
}