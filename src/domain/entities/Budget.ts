import generateGuid from "../../core/utils/functions";

export default class Budget {
    public id: string;
    public name: string;
    public userId: string;

    constructor({ id, name, userId }: BudgetProps) {
        this.id = id ?? generateGuid();
        this.name = name;
        this.userId = userId;
    }
}

interface BudgetProps {
    id?: string;
    name: string;
    userId: string;
}