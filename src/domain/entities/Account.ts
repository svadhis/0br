import generateGuid from "../../core/utils/functions";
import Transaction from "./Transaction";

export default class Account {
    public id: string;
    public name: string;
    public type: AccountType;
    public startingBalance: number;
    public transactions: Transaction[];

    constructor({ id, name, type, startingBalance, transactions }: AccountProps) {
        this.id = id ?? generateGuid();
        this.name = name;
        this.type = type;
        this.startingBalance = startingBalance ?? 0;
        this.transactions = transactions ?? [];
    }

    public addTransaction(amount: number): void {
        this.transactions.push(new Transaction({ amount: amount }));
    }

    public getBalance(): number {
        return this.transactions.reduce((acc, curr) => acc + curr.amount, this.startingBalance);
    }
}

interface AccountProps {
    id?: string;
    name: string;
    type: AccountType;
    startingBalance?: number;
    transactions?: Transaction[];
}

export enum AccountType {
    BANK = "BANK",
    CASH = "CASH",
    CREDIT_CARD = "CREDIT_CARD",
}