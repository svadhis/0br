import generateGuid from "../../core/utils/functions";
import Budget from "./Budget";

export default class User {
    public id: string;
    public email: string;
    public budgets: Budget[];

    constructor({ id, email, budgets }: UserProps) {
        this.id = id ?? generateGuid();
        this.email = email;
        this.budgets = budgets ?? [];
    }

    public addBudget(budget: Budget): void {
        this.budgets.push(budget);
    }

    public removeBudget(budget: Budget): void {
        this.budgets = this.budgets.filter(b => b.id !== budget.id);
    }
}

interface UserProps {
    id?: string;
    email: string;
    budgets?: Budget[];
}
