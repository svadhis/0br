import generateGuid from "../../core/utils/functions";
import Transaction from "./Transaction";

export default class MonthlyBudget {
    public id: string;
    public expenseCategoryId: string;
    public month: Month;
    public transactions: Transaction[];
    public assignment: number;
    public estimation: number | undefined;
    public payeeId: string | undefined;
    public accountId: string | undefined;

    constructor({ id, expenseCategoryId: expenseCategoryId, month, assignment, estimation, transactions, payeeId, accountId }: MonthlyBudgetProps) {
        this.id = id ?? generateGuid();
        this.expenseCategoryId = expenseCategoryId;
        this.month = month;
        this.transactions = transactions ?? [];
        this.assignment = assignment ?? 0;
        this.estimation = estimation;
        this.payeeId = payeeId;
        this.accountId = accountId;
    }
}

interface MonthlyBudgetProps {
    id?: string;
    expenseCategoryId: string;
    month: Month;
    assignment?: number;
    estimation?: number;
    transactions?: Transaction[];
    payeeId?: string;
    accountId?: string;
}

export enum Month {
    JANUARY = 'JANUARY',
    FEBRUARY = 'FEBRUARY',
    MARCH = 'MARCH',
    APRIL = 'APRIL',
    MAY = 'MAY',
    JUNE = 'JUNE',
    JULY = 'JULY',
    AUGUST = 'AUGUST',
    SEPTEMBER = 'SEPTEMBER',
    OCTOBER = 'OCTOBER',
    NOVEMBER = 'NOVEMBER',
    DECEMBER = 'DECEMBER'
}